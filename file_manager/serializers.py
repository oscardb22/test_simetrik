from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from .models import UploadLog
from datetime import datetime
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
import pandas as manager_pd


class SerializerUploadLog(serializers.ModelSerializer):

    class Meta:
        model = UploadLog
        fields = ['file_name', ]

    def validate(self, attrs):
        file_name = attrs.get('file_name')
        if file_name.name[-3:] != 'csv':
            raise serializers.ValidationError(_('this file extension is not allow'), code='authorization')
        data = manager_pd.read_csv(file_name, delimiter=',')
        columns = data.columns
        if 'transaction_id' not in columns:
            raise serializers.ValidationError(_('the column *transaction_id* is required'), code='authorization')
        if 'transaction_date' not in columns:
            raise serializers.ValidationError(_('the column *transaction_date* is required'), code='authorization')
        if 'transaction_amount' not in columns:
            raise serializers.ValidationError(_('the column *transaction_amount* is required'), code='authorization')
        if 'client_id' not in columns:
            raise serializers.ValidationError(_('the column *client_id* is required'), code='authorization')
        if 'client_name' not in columns:
            raise serializers.ValidationError(_('the column *client_name* is required'), code='authorization')
        return attrs


class SerializerListUploadLog(serializers.ModelSerializer):

    class Meta:
        model = UploadLog
        fields = ['file_name', 'date_upload']


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(
        label=_("CellPhone, Email or Username"),
        trim_whitespace=True
    )
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(
                request=self.context.get('request'),
                username=username,
                password=password
            )

            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')

            if not user.is_active:
                msg = _('User is not active.')
                raise serializers.ValidationError(msg, code='User is not active')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        user.last_login = datetime.now()
        user.save()
        list_token = Token.objects.filter(user=user)
        if list_token.exists():
            list_token.delete()
        token = Token.objects.create(user=user)
        attrs['token'] = token
        return attrs
