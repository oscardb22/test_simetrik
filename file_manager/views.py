from rest_framework.decorators import permission_classes
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .serializers import AuthTokenSerializer, SerializerListUploadLog, UploadLog, SerializerUploadLog
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.status import HTTP_201_CREATED


class CustomAuthToken(APIView):
    serializer_class = AuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data['token']
        return Response({
            'token': token.key,
        })


@permission_classes([IsAuthenticated, ])
class Exit(APIView):
    http_method_names = ['get', ]

    def get(self, request):
        if request.user:
            Token.objects.filter(user=request.user).delete()
        return Response({
            'detail': 'Te esperamos pronto.',
        })


@permission_classes([IsAuthenticated, ])
class ListUploadLog(ListAPIView):
    serializer_class = SerializerListUploadLog
    queryset = UploadLog.objects.all()
    http_method_names = ['get', ]


@permission_classes([IsAuthenticated, ])
class CreateUploadLog(CreateAPIView):
    serializer_class = SerializerUploadLog
    queryset = UploadLog.objects.all()
    http_method_names = ['post', ]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
