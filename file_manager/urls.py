from django.urls import path
from .views import CustomAuthToken, Exit, ListUploadLog, CreateUploadLog

urlpatterns = [
    path('app/sign_in/', CustomAuthToken.as_view()),
    path('app/sign_out/', Exit.as_view()),
    path('list/files/', ListUploadLog.as_view()),
    path('create/files/', CreateUploadLog.as_view()),
]
