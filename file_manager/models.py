from django.db import models
from django.conf import settings


class UploadLog(models.Model):
    file_name = models.FileField(upload_to='upload_file')
    date_upload = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"{self.file_name.name}, {self.date_upload}"
