## Tables

| order | Endpoint | Description |
| ----- | -------- | ----------- |
|1|http://localhost:8000/file/app/sign_in/|this endpoint make authentication and return the transaction token|
|2|http://localhost:8000/file/app/sign_out/|this endpoint remove transaction token and exit the user|
|3|http://localhost:8000/file/list/files/|this endpoint return list data register|
|4|http://localhost:8000/file/create/files/|this endpoint create the register in the database and review whit pandas the columns in the file |

# Instructions

1. create a virtualenv (optional)

    `virtualenv -p python3.8 env`
    
    `source env/bin/active`
2. install requirements of the requirements.txt file

    `pip install -r requirements.txt`
3. execute the commands makemigrations y migrate for create the BD

    `python manage.py makemigrations file_manager`
    
    `python manage.py makemigrations`
    
    `python manage.py migrate`
3. Run Django

    `python manage.py runserver`
3. Run Django

    `python manage.py runserver`
---
**NOTE**

all transaction need the authentication header token

`Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b`

```json
{
"file_name": "file" 
}

```
---