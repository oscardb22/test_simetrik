from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('file/', include(('file_manager.urls', 'file'), namespace='file')),
]
